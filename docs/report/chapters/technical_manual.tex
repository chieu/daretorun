\chapter{Technical manual}
% ------------------- SECTION -------------------
\section{Design and development choices}

% ---
\subsection{Design}

Being a small computer, the \textbf{Raspberry Pi 4 Model B} is the hardware used for this project. 
Its minimal size and affordable price help achieve the requirements of the project.
In addition to the Raspberry Pi, a camera \cite{camera_module} and a headphone are also used.  

\noindent Here is a list of the main components and their roles:
\begin{itemize}[topsep=-1.5em]\setlength\itemsep{-0.75em}
    \item \textbf{OS} (\texttt{Raspberry Pi OS}): Manages hardware and software
    \item \textbf{CPU}: Executes code (\texttt{MATLAB \& SIMULINK executable})
    \item \textbf{Storage} (\texttt{Micro SD Card}): Stores files (\texttt{MATLAB libraries and packages})
    \item \textbf{RAM}: Stores temporary data from processes (\texttt{Image captured from the camera})
    \item \textbf{Camera}: Captures images (\texttt{Line detection})
    \item \textbf{Headphone}: Outputs audio (\texttt{Audio feedback})
\end{itemize}

\newblock
\begin{figure}[ht]
    \centering
    \subfloat[\centering Prototype]{
        \includegraphics[height=5cm, keepaspectratio]{images/technical_report/device.png}
    }
    \qquad\qquad
    \subfloat[\centering Hardware structure]{
        \includegraphics[height=5cm, keepaspectratio]{images/technical_report/design.png}
    }
    \caption{Design}
\end{figure}

% ---
\subsection{Development choices}
The device needs to \textbf{detect and follow a line} added to guiding its user with \textbf{audio feedback} indicating the correct direction.

\subsubsection{Line detection}
After the image acquisition and its pre-processing to remove noise, we obtain a binary image.
Assuming that the contrast between the line and the ground is very high, there are two ways to detect a line in the binary image:
\begin{itemize}[topsep=-1.5em]\setlength\itemsep{-0.5em}
    \item using \textbf{Hough transform} \cite{hough}
    \item using \textbf{Line middle detection}
\end{itemize}

\newpage
The \textbf{Hough transform} is the classic way to detect a line in a image by
computing its associated \textbf{Hough space}.
\textbf{Points} in the \textbf{image space} which have the color of the line are represented by a \textbf{sine} in the \textbf{Hough space}.

\begin{center}
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Image space} & \textbf{Hough space} \\
        \hline
        Point & Sine \\
        Line (intersection of two \textbf{points}) & Point (intersection of two \textbf{sines}) \\
        \hline
    \end{tabular}
\end{center}

The more sine intersections there are in the Hough space, the more points belong to a line in the image one. 

Given that a line can be defined by its equation $\rho = x cos\theta + y sin\theta$, we can get the \textbf{longest line} by finding $\rho$ and $\theta$ associated to the \textbf{maximum value} in the Hough space.

\begin{figure}[ht]
    \centering
    \includegraphics[height=6cm]{images/technical_report/hough_space.png}
    \caption{Hough space associated to the image space}
\end{figure}

In addition, detecting only the longest line can eliminate obstacles that can be mistaken for the correct path, since they are excluded because of their minimal dimensions (compared to the line). (See the figure below).

\begin{figure}[h]
    \centering
    \includegraphics[height=5.5cm]{images/technical_report/hough_detection.png}
    \caption{Hough detection without Canny filter (Wall Ignored)}
\end{figure}

Usually, the \textbf{Canny edge filter} \cite{canny_edge} is used to detect the edges of the image before the Hough transform.
However, if the line is curved and the Canny edge is applied before the Hough transform, the longest \textbf{straight} line detected will only represent a portion of the curve as pointed out in Figures 2.3 and 2.4.
In the worst-case scenario, the obstacle will be detected instead.

\begin{figure}[ht]
    \centering
    \includegraphics[height=5.5cm]{images/technical_report/hough_canny_detection.png}
    \caption{Hough detection with Canny filter (Wall Detected)}
\end{figure}

\newpage
The \textbf{Line middle detection} is an easy way to find a line/curve but only works in an image with very few colors (ideally two).
Indeed, this method assumes that we know the color of the line so the less color there is, the better it is.
For each row of the image, we find every pixel that match the color of the line.
Then, we consider that the middle pixel among the found ones is a point of the line.
At the end, we will reach every row of the image and therefore, obtain the line/curve.
This method differs from the Hough transform in its simplicity and its ability to find any line/curve.

The main issue of this method is the presence of obstacles which returns a distorted curve.
We can reduce the impact of obstacles by validating the obtained line/curve.
To do so, we fit the obtained line/curve with a polynomial and compute the error estimation (norm of residuals).
If the error estimation is less than a predefined threshold, then we assume that the line/curve is valid, otherwise the line is discarded.

\begin{figure}[ht]
    \centering
    \includegraphics[height=5.5cm]{images/technical_report/color_based_detection.png}
    \caption{Line middle detection with polynomial fit verification (Wall Detected)}
\end{figure}

Figure 2.5 highlights the salient features of the Line Middle technique which detects the line but also the wall.
In a nutshell, both methods struggle to ignore obstacles which have the same color as the line.
However, \textbf{Hough transform detection} can ignore some obstacles whereas \textbf{Line middle detection} can only discard the line.

\newpage
\subsection{Selecting the direction}

\noindent Once the line is detected, we need to guide the user. To do this, we decided to treat three cases:
\begin{enumerate}\setlength\itemsep{-0.75em}
        \item The user is \textbf{on the line}: They have to \textbf{move forward}.
    \item The user is \textbf{on the left side} of the line: They have to \textbf{move right}.
    \item The user is \textbf{on the right side} of the line: They have to \textbf{move left}.
\end{enumerate}

\noindent The \textbf{orthogonal projection} \cite{orthogonal_projection} of the center point to the line allows us to find the direction.

Let $P_1(r_1, c_1)$ and $P_2(r_2, c_2)$ the extremities of the line and
$P_c(r_c, c_c)$ the center of the image. (\textit{(r, c): (row, column)})

\begin{figure}[ht]
    \centering
    \includegraphics[height=6.5cm]{images/technical_report/orthogonal_projection.png}
    \caption{Orthogonal projection}
\end{figure}

\noindent Let's find the coordinate of $P_p(r_p, c_p)$ the projection of the center point to the line.

\begin{align}
    \begin{split}
        \overrightarrow{P_1P_p} ={}& \frac{\overrightarrow{P_1P_c} \cdot \overrightarrow{P_1P_2}}{{||\overrightarrow{P_1P_2}||}^{2}} \cdot \overrightarrow{P_1P_2}\\
                                ={}& \frac{(r_c-r_1)(r_2-r_1)+(c_c-c_1)(c_2-c_1)}{(r_2-r_1)^{2}+(c_2-c_1)^{2}} \cdot \overrightarrow{P_1P_2}\\
                                ={}& K \cdot \overrightarrow{P_1P_2}
    \end{split}
\end{align}

\begin{equation}
    \begin{pmatrix}r_p-r_1 \\ c_p-c_1\end{pmatrix}
    =
    K \cdot \begin{pmatrix}r_2-r_1 \\ c_2-c_1\end{pmatrix}
\end{equation}

$$
\left\{
    \begin{array}{ll}
        r_p = K(r_2-r_1)+r_1 \\
        c_p = K(c_2-c_1)+c_1
    \end{array}
\right.
$$

With the point $P_p$, we can compute the distance ($d$) to the center and the position compared to the line ($sign(c_p-c_c)$):
\begin{itemize}\setlength\itemsep{-0.75em}
    \item if $d < delta$ then \textbf{move forward};
    \item elif $d > delta$ and $c_p < c_c$ (projected point is on the left side) then \textbf{move right};
    \item elif $d > delta$ and $c_p > c_c$ (projected point is on the right side) then \textbf{move left};
    \item else wait until the next instruction.
\end{itemize}

\newpage
\subsubsection{Audio feedback}

Once the line is detected, the user needs to be guided. This will be performed by playing a specific sound to indicate the correct direction to be chosen.

We tried to vary the amplitude of the sound but the difference was barely discernible.
Therefore, we decided to adjust the frequency which provides a clear output to the user. We chose the following frequencies to indicate which direction to follow.

\begin{center}
    \begin{tabular}{|c|c|}
        \hline
        Direction & Sound \\
        \hline
        Forward & 440 Hz \\
        Left & 220 Hz \\
        Right & 660 Hz \\
        \hline
    \end{tabular}    
\end{center}


% ------------------- SECTION -------------------
\section{Software architecture and computational complexity}

\subsection{Software architecture}
\begin{figure}[h]
    \centering
    \includegraphics[width=17cm]{images/technical_report/SIMULINK_model.png}
    \caption{MATLAB \& SIMULINK model}
\end{figure}

The software can be split into four main components:
\begin{itemize}\setlength\itemsep{-0.75em}
    \item Video input (1)
    \item Line detection (2)
    \item Video output (2)
    \item Audio output (4)
\end{itemize}

\noindent \textbf{Video input} acquires images and returns only RGB channels. Therefore, we create a single image array by concatenating them.

\noindent \textbf{Line detection} detects the line using the Hough transform (\textit{getLinePoints}) and finds the direction with the orthogonal projection (\textit{getDirectionFromLine}).

\noindent \textbf{Video output} displays the image with the detected line. It is mainly used for the initial setup and debugging.

\noindent \textbf{Audio output} plays a sound according to the direction to guide the user.

\noindent \textit{Detailed documentation of each function in the model can be found \href{https://gitlab.eurecom.fr/chieu/daretorun/-/blob/main/src/HoughDetection.slx}{in the SIMULINK model}}.

% ---
\subsection{Computational complexity}

\subsubsection{Time complexity}
Let $n=max(R, C)$ where $R$ is the number of rows and $C$ the number of columns of the image.
\begin{align}
    \begin{split}
        T_{Video\_input}(n) ={}& T_{video\_capture}(n) + T_{rgb2im}(n)\\
                            ={}& O(1) + O(n^{2})\\
                            ={}& O(n^{2})
    \end{split}\\
    \begin{split}
        T_{Line\_detection}(n)  ={}& T_{getLinePoints}(n) + T_{getDirectionFromLine}(n)\\
                                ={}& O(n^{3}) + O(1)\\
                                ={}& O(n^{3})
    \end{split}\\
    \begin{split}
        T_{Video\_output}(n)    ={}& T_{im2rgb}(n) + T_{draw\_line}(n)\\
                                ={}& O(1) + O(n)\\
                                ={}& O(n)
    \end{split}\\
    \begin{split}
        T_{Audio\_output}(n)    ={}& T_{volumeControl}(n)\\
                                ={}& O(n)
    \end{split}\\
    \begin{split}
        T_{Model}(n) ={}& T_{Video\_input}(n) + T_{Line\_detection}(n) + T_{Video\_output}(n) + T_{Audio\_output}(n)\\
                     ={}& O(n^{2}) + O(n^{3}) + O(n) + O(n)\\
                     ={}& O(n^{3})
    \end{split}
\end{align}

\subsubsection{Space complexity}
We keep the same notations as the previous section 2.2.2.1.
Let $f$ be the sampling rate.  
\begin{align}
    \begin{split}
        S_{Model}(n) ={}& max(n^{2}, 2f)
    \end{split}
\end{align}
(image or stereo sound arrays)
% ------------------- SECTION -------------------
\section{Testing and verification}
We \textbf{tested} our prototype on several types of curves and obstacles and \textbf{verified} its accuracy and reliability by checking whether the audio output corresponds to the actual correct direction the user must choose. 


\begin{figure}[ht]
    \centering
    \includegraphics[height=4cm]{images/technical_report/test.png}
    \caption{Local testing}
\end{figure}  


After conducting tests using blindfolds, we concluded that the prototype provides the necessary guidance to the user who follows the white line with diminutive missteps.
The prototype works well and meets our expectations.

% ------------------- SECTION -------------------
\newpage
\section{Discussion}
Despite the fact that DARE TO RUN promises outstanding performance and ensures exceptional results, the undertaking has also encountered some adversities en route.   

At first, we wanted the audio feedback to be spoken sentences saying "left", "right" or "forward" depending on the direction to follow but on SIMULINK, we could not implement them because of the infinite loop. In fact, the sentences would keep being repeated and would not change fast enough to keep up with the images. Therefore, we decided to use a continuous sound that would change frequency depending on where the user's location compared to  the line. The user will be guided by a higher sound indicating the correct direction: 
If the line is on the user's right, audio will be output in the right earphone, and vise - versa if the correct direction is on the user's left. Furthermore, if they are on the correct path, both earphones will yield continuous audio output.

However, the method that we used to detect lines is not flawless. Indeed, if the camera detects an obstacle near the line, the processing might mistake this obstacle for the line itself and thus, gives wrong directions to the user. An answer to that issue could be an advanced AI implementation that would differentiate obstacles from the line.

Moreover, in order to speed up the processing, the image's quality can be reduced.

To have an optimal use of our device, the camera needs to be facing the ground and remain stable. Therefore, the user has to utilize a contraption to attach it to their waist. We were intending to print a 3D box that we would then attach to an elastic with a buckle to put around the waist. However, we were not able to do this due to a lack of financial resources and dedicated budget for the design. 