# Raspberry Pi setup guide

## Prerequisite

This guide allows you to install everything you need for the Raspberry Pi making everything possible using wireless connection.

Materials needed:
- Raspberry Pi 4
- Micro SD Card (minimum 8 GB)
- Micro SD Card to SD Card adapter
- Camera module
- Internet connection (mobile access point, personal wifi)

---
## Prepare Mathworks Raspbian image
Download the Mathworks Raspbian R22.2.20 image [**here**](https://github.com/mathworks/Raspbian_OS_Setup/releases/tag/R22.2.0).

This image contains all the packages and libraries needed for MATLAB & Simulink.

Once downloaded, decompress the zip file.

![Unzip](./assets/matworks-raspbian-unzip.png)

Install RPI Imager ([**Download here**](https://github.com/raspberrypi/rpi-imager/releases/latest)) to write this image to the SD Card.

<div align=center>

|Linux|Windows|Mac|
|---|---|---|
|.deb|.exe|.dmg|

||
|:-:|
|*Extension of the installer according to your operating system*|
</div>

---
## Install Raspberry Pi OS on Micro SD card

- Plug your Micro SD Card to your PC

- Start RPI Imager as administrator or root (`sudo rpi-imager`)
  
  ![RPI Imager](./assets/rpi-imager.png)

- Format the SD card as FAT32

  ![RPI Imager choose OS](./assets/rpi-imager-choose-os.png)

  ![RPI Imager format](./assets/rpi-imager-format-card.png)

- Write
  
  ![RPI Imager write](./assets/rpi-imager-format-card-write.png)

- Select the previously downloaded image (*mathworks_raspbian_R22.2.0.img*)

  ![RPI Imager install](./assets/rpi-imager-choose-os.png)

  ![RPI Imager custom os](./assets/rpi-imager-custom-os.png)

  ![RPI Imager Mathworks Raspbian](./assets/rpi-imager-mathworks-raspbian.png)

- Open *Advanced options*
  
  ![RPI Imager options](./assets/rpi-imager-options.png)

- Fill the blanks (here an example):
    - **Hostname**: `michel.local`
    - **Enable SSH**: Checked
      - **Use password authentification**: Checked
    - **Set username and password**:
      - Username: `pi`
      - Password: `password`
    - **Configure wireless LAN**:
      - **SSID**: `YOUR_WIFI_SSID`
      - **Password**: `YOUR_WIFI_PASSWORD`

  ![RPI Imager options examples](./assets/rpi-imager-options-examples.png)

- Finally write Mathworks Raspbian R22.2.0 to the SD card
  
  ![RPI Imager write rpi os](./assets/rpi-imager-raspberry-pi-os-write.png)

  ![RPI Image write completed](./assets/rpi-imager-raspberry-pi-os-write-completed.png)


Now you can plug the Micro SD Card in the Raspberry Pi and connect to it using SSH or VNC.

---
## First boot

The first boot may take more time than subsequent ones. In fact, you need to wait 3-5 minutes to make sure everything is ready (especially regarding wireless connection).

**If after 5 minutes, the Raspberry Pi didn't connect to your wireless LAN, please plug it to a screen and connect to it manually.**

Once your Raspberry Pi is connected to the wireless LAN, you can connect to it wirelessly through SSH with the command `ssh pi@YOUR_HOSTNAME` using a device connected to the same wireless LAN as the Raspberry Pi

Example:
```
$ ssh pi@michel.local
pi@michel.local's password: [TYPE YOUR PASSWORD]
```

Type the command `hostname -I` to get the **local IP address** of your Raspberry Pi:
```
pi@michel:~ $ hostname -I
192.168.182.28
```
Here the local IP is `192.168.182.28`.

Now you can connect to your Raspberry Pi through SSH using:
  - **Hostname**: `ssh pi@michel.local` (do not depend on the wireless LAN)
  - **Local IP address**: `ssh pi@192.168.182.28` (do depend on the wireless LAN)

---
## Camera

Plug the camera module as shown here:

![Connect camera](assets/connect-camera.gif)

---
## Next step
[**Install dependencies for the Raspberry Pi and the PC**](/docs/INSTALLATION.md)