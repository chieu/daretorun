# Installation guide

## Install Raspberry Pi add-on for MATLAB & Simulink

Install required add-ons for MATLAB & Simulink:

- **Audio Toolbox** (from Add-on manager only)

- **Computer Vision Toolbox** (from Add-on manager only)

- **Raspberry Pi Support from MATLAB**: 
  
  [![](https://img.shields.io/badge/MATLAB%20%26%20Simulink-Get%20Add--on-blue)](https://mathworks.com/hardware-support/raspberry-pi-matlab.html)

- **Raspberry Pi Support from Simulink**: 
  
  [![](https://img.shields.io/badge/MATLAB%20%26%20Simulink-Get%20Add--on-blue)](https://mathworks.com/hardware-support/raspberry-pi-simulink.html)

  |![MATLAB add-on raspberry](./assets/matlab-add-on-raspberry.png)|
  |:-:|
  |*What you should see in your MATLAB add-on manager*|

**For Windows users**:

- **MATLAB Support for MinGW-w64 C/C++ Compiler**: 
  - [![](https://img.shields.io/badge/MATLAB%20%26%20Simulink-Get%20Add--on-blue)](https://mathworks.com/matlabcentral/fileexchange/52848-matlab-support-for-mingw-w64-c-c-compiler)

**For Mac users**

- Get [Xcode](https://apps.apple.com/fr/app/xcode/id497799835?mt=12) to install the required compilers.

---
## Next step

[**Deploy on a Raspberry Pi**](/src/RUN.md)