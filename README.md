<div align="center">
<img src="./assets/daretorun.png" align="center" width="250" alt="DareToRun icon">

# DareToRun

*Assistive device for visually impaired people using **Raspberry Pi***

---
## Software and hardware 

<img src="./assets/raspberry-pi.png" align=center height=50px width=auto> ***Raspberry Pi 4 Model B***

<img src="./assets/matlab.png" align=center height=50px width=auto> ***MATLAB & Simulink***

---
## Academic project page
[***s5project***](https://gitlab.eurecom.fr/renaud.pacalet/s5project)

---
## Project website
[***Website***](https://maleksfaxi.github.io/DareToRun/)

---
## Report
[**PDF**](/docs/report/main.pdf)

---
## Python git project page
[***DareToRun-Python***](https://gitlab.eurecom.fr/chenv/daretorun-python)

---
## Supervisors
[Renaud Pacalet]

[Chiara Galdi]

[Massimiliano Todisco]

[Melek Önen] (Tutor)

---
## Team members
[Villon Chen]

[William Chieu]

[Madleen Jean]

[Elena Lassalle]

[Malek Sfaxi]

</div>

---
## Overview
The objective of the project S5 is to create a device allowing visually impared people to follow a line. 

The device we came up with is composed of a Rasberry Pi connected to earphones which gives audio output and to a camera which can detect the line.

---
## Guides
1. Setup the Raspberry Pi ([**GUIDE**](/docs/SETUP.md))

2. Install MATLAB & Simulink dependencies for the PC ([**GUIDE**](/docs/INSTALLATION.md))

3. Deploy on a Raspberry Pi ([**GUIDE**](/src/RUN.md))
   
---
## Testing
Test the MATLAB & Simulink model without Raspberry Pi ([**GUIDE**](/test/RUN.md))

---
## Poster

![Poster](/docs/assets/poster.png)

---
## Project structure
```
.
├── assets                          # Static files
|
├── docs                            # Documentation files
|   ├── assets
|   ├── report                      # Report LaTex source code
|   |   ├── report.pdf              # Compiled pdf
|   |   └── ...
|   ├── assets                      
|   ├── poster.pdf                  # Poster
|   ├── SETUP.md                    # Guide to setup the Raspberry Pi
|   └── INSTALLATION.md             # Guide to install dependencies  
|
├── src                             # Source code (for Raspberry Pi)
|   ├── assets 
|   ├── HoughDetection.slx          # MATLAB & Simulink model
|   └── RUN.md                      # Guide to run the main model
|
├── test                            # Testing code (for PC)
|   ├── assets 
|   ├── localRunHoughDetection.slx  # MATLAB & Simulink model
|   └── RUN.md                      # Guide to run the test model
|
├── README.md
|
└── .gitignore
```

<!--- Contacts -->
[Renaud Pacalet]: mailto:Renaud.Pacalet@telecom-paris.fr
[Chiara Galdi]: mailto:Chiara.Galdi@eurecom.fr
[Massimiliano Todisco]: mailto:Massimiliano.Todisco@eurecom.fr
[Melek Önen]: mailto:Melek.Onen@eurecom.fr

[Villon Chen]: mailto:Villon.Chen@eurecom.fr
[William Chieu]: mailto:William.Chieu@eurecom.fr
[Madleen Jean]: mailto:Madleen.Jean@eurecom.fr
[Elena Lassalle]: mailto:Elena.Lassalle@eurecom.fr
[Malek Sfaxi]: mailto:Malek.Sfaxi@eurecom.fr
