# Run on Raspberry Pi

*This guide assumes that your Raspberry Pi is already configured.*

*If not please follow this [**guide**](/docs/SETUP.md).*

## MATLAB & Simulink model

![Hough detection model](./assets/HoughDetection.svg)

---
## Setup

1. Download the model: [HoughDetection.slx](./HoughDetection.slx)
2. Open the model in MATLAB & Simulink
3. **CTRL-E** to open *Configuration Parameters* then *Hardware Implementation*
    - *Hardware board*: **Raspberry Pi**
    - Fill the blanks for *Device Address*, *Username* and *Password*
  
    ![Configuration Parameters](./assets/Configuration_Parameters.png)

4. Select the color of the line to be detected
   
    ![Select color](./assets/select_color.png)

5. Change frequencies for the sound (optional)

6. Select *Hardware* in the toolstrip and ***Build*** only
   
   ![Toolstrip](./assets/toolstrip.png)

   ![Build only](./assets/build_only.png)

7. Wait until it compiles successfully
   
   ![Build successful](./assets/build_successful.png)

8. Start VNC session ([Install VNC Viewer](https://www.realvnc.com/fr/connect/download/viewer/))
    - Type the IP address of your Raspberry Pi 
    - Put your credentials

    ![VNC connection](./assets/vnc_connection.png)

9.  Start a terminal
   
   ![VNC terminal](./assets/vnc_terminal.png)

10. Run the commands to start the program

    ```bash
    # Change directory
    $ cd MATLAB/R2022b  # Replace 'R2022b' with your version

    # Start the Hough detection model
    $ sudo ./HoughDetection # sudo is required
    ```

11. Plug the headphone

12. Place the device at waist level

Congratulation, the model has been deployed successfully on your Raspberry Pi. 

---
## Demo
![VNC Demo](./assets/vnc_demo.png)