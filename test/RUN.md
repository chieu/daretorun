# Run on PC (testing)

*This guide assumes that your PC has already installed Raspberry Pi add-on for MATLAB & Simulink.*

*If not please follow this [**guide**](/docs/INSTALLATION.md#install-raspberry-pi-add-on-for-matlab--simulink). (**Install Raspberry Pi add-on for MATLAB & Simulink** part only)*

## MATLAB & Simulink model

![Hough detection test model](./assets/localRunHoughDetection.svg)

---
## Setup

1. Download the model: [localRunHoughDetection.slx](./localRunHoughDetection.slx)
2. Open the model in MATLAB & Simulink

3. Select the color of the line to be detected
   
    ![Select color](./assets/select_color.png)

4. Change frequencies for the sound (optional)

5. Select *Simulation* in the toolstrip and ***Run***
   
   ![Toolstrip](./assets/toolstrip.png)

   ![Build only](./assets/run_model.png)

---
## Demo
![Run](./assets/run.gif)